// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookieParser = require('cookie-parser');
var crypto = require('crypto');

//var Global
var salt = "&é&9 çù,;d<w*l*hc$*bbd98gf ||dfb^^87";

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

//homepage
app.get("/", function(req, res, next){
	
});

//Affiche le contenu de la table users
app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

//Affiche le contenu de la table sessions
app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

//Supprime la table sessions
app.get("/clearSession", function(req, res, next) {
	db.run('DELETE FROM sessions;');
	res.redirect('/sessions');
});

//supprime la session courante de la table sessions
app.post("/deconnexion", function(req, res, next) {
	if(typeof req.headers.cookie != "undefined"){
		var token = req.headers.cookie.split("=")[1];
	}
	db.run('DELETE FROM sessions WHERE token = ?;',token);
	res.send('', 200);
});

app.use(cookieParser());

//Process pour le login
app.post("/login", function(req, res, next) {

	var id = (typeof req.body.id !== 'undefined') ? req.body.id : false ;
	var mdp = (typeof req.body.mdp !== 'undefined') ? req.body.mdp : false ;
	if(id && mdp){
		var hash = crypto.createHash('md5').update(salt+mdp).digest('hex');
		db.get('SELECT ident, password FROM users WHERE ident = ? AND password = ?',[id, hash], function(err,rows){
		  	if(typeof rows !== 'undefined'){

				//Perform INSERT operation.
				var token = Math.random().toString(36).substr(2)+ Date.now() + Math.random().toString(36).substr(2) + "_" +id;

				updateSession(id, token);
				
				
				var data = {'status' : true, 'token' : token, 'id' : id};
				
				res.cookie('login', token, { maxAge: 900000, httpOnly: true });
				res.json(data);	
			}else{
				res.json({'status' : false});	
			};	


		}); 
	}else{
		res.json({'status' : false});	
	}
});

/**
 * Update la table session après le login si user deja present sinon insert
 */
function updateSession(id, token){
	db.get('SELECT ident FROM sessions WHERE ident = ?',[id], function(err,rows){
		  	if(typeof rows !== 'undefined'){
				db.run('UPDATE sessions SET token = ? WHERE ident= ?',[token, id]);
			}else{
				db.run('INSERT into sessions(ident,token) VALUES (?,?)',[id, token]);
			}

		});
}

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
