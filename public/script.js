$(document).ready(function() {
	
	//cree un cookie
	function setCookie(c_name, value, exdays) {
	    var exdate = new Date();
	    exdate.setDate(exdate.getDate() + exdays);
	    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	    document.cookie = c_name + "=" + c_value;
	}

	//recupere la valeur d'un cookie
	function getCookie(c_name) {
	    var i, x, y, ARRcookies = document.cookie.split(";");
	    for (i = 0; i < ARRcookies.length; i++) {
	        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
	        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
	        x = x.replace(/^\s+|\s+$/g, "");
	        if (x == c_name) {
	            return unescape(y);
	        }else{
				return false;		
			}
	    }
	}

	// supprime un cookie
	function deleteCookie(c_name){
		setCookie(c_name, "", -1);
	}
	
	// Process de verif si user deja connecté
	var id_user = getCookie("loginAjax");
	if(id_user != false){
		$("#home").html(welcome(getCookie("loginAjax").split("_")[1]));
	}

	//template message bienvenue
	function welcome(id){
		var html ='<div class="panel panel-default">\
					  		<div class="panel-body text-center">\
							  <span class="vertical-align h2">Bonjour '+ id +' !</span>\
							<span class="pull-right"><a id="btnDeco" class="btn btn-info" href="/deconnexion">Déconnexion</a></span>\
					  		</div>\
						</div>';
		return html;
	}

	//template message deconnexion
	function ciaossu(){
		var html ='<div class="panel panel-default">\
					  		<div class="panel-body text-center">\
							<img class="img-responsive img-thumbnail" src="img/ciaossu_reborn.png"/>\
					  		</div>\
						</div>';
		return html;
	}

	//template deconnexion en cours
	function loadingDeconnexion(){
		var html ='<div class="panel panel-default">\
					  		<div class="panel-body text-center">\
							  <span class="vertical-align h2"><img class="loading" src="img/loading.gif" /> Déconnexion en cours...</span>\
					  		</div>\
						</div>';
		return html;
	}

	//Action pour la connexion	
	$("#btnSend").on('click', function(event){
		event.preventDefault();
		   $.ajax({
				  method: "POST",
				  url: "/login",
				  data: { id: $("#form_id").val(), mdp: $("#form_pwd").val() }
				})
				.done(function(msg, status, xhr) {
					if(!msg.status){
						$('div[target="has-error"]').addClass("has-error");
						$('div[target="has-error"]').removeClass("has-success");
						openModal(false);
					}else{
						setCookie("loginAjax", msg.token , 2);
						$('div[target="has-error"]').removeClass("has-error");
						$('div[target="has-error"]').addClass("has-success");
						openModal(true);
						$("#home").html(welcome(msg.id));
						console.log(document.cookie);
				}
		   });
	});


	//template pour ouvrir un modal
	function openModal(success){
		var title= "";
		var msg= "";
		if(success){
		 title= "Connexion réussie";
		 msg= "Vous êtes maintenant connecté au site.";
		}else{
		 title= "Echec de la connexion";
		 msg= "Les identifiants ne sont pas valides !";
		}
		var html_modal = '<div id="modal_error" class="modal fade" tabindex="-1" role="dialog">\
		  <div class="modal-dialog" role="document">\
			<div class="modal-content">\
			  <div class="modal-header">\
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
			    <h4 class="modal-title">'+ title +'</h4>\
			  </div>\
			  <div class="modal-body">\
			    <p>'+ msg +'</p>\
			  </div>\
			  <div class="modal-footer">\
			    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>\
			  </div>\
			</div><!-- /.modal-content -->\
		  </div><!-- /.modal-dialog -->\
		</div><!-- /.modal -->';
		$('body').append(html_modal);
		$('#modal_error').modal();
		$('#modal_error').on('hidden.bs.modal', function (e) {
		 $(this).remove();
		});
	}

	//Action pour la deconnexion
	$(document).on('click', '#btnDeco', function(e){
		e.preventDefault();
		$("#home").html(loadingDeconnexion());
	
		$.ajax({
				  method: "POST",
				  url: "/deconnexion",		
				  })
				.done(function( msg ) {
					deleteCookie("loginAjax");
					$("#home").html(ciaossu());
					 setTimeout(function(){ document.location.href="/" }, 2000);
	  		   });
	});


}); //$(document).ready
